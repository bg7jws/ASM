8086 MASM
16bit dos 汇编，代码可以在16位和32位操作系统下运行。

主要是2020年COVID-19期间为大学计算机专业辅导的习题和大作业。希望大学生能够获得正确和准确的知识，并能够理解消化。
部分程序有非常详细的注释，可用于8086汇编入门练习写简单程序的参考。
大部分程序都比较简单粗糙，甚至有可能有很大瑕疵。编程环境用的是Radasm 2.2.1.9
从2020年8月31日不再维护，或许零星加入一些短小程序，但是不再进行代码更改了。
